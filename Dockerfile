FROM python:3.9-slim-buster

LABEL Name="Python Flask Demo App" Version=1.4.2
LABEL org.opencontainers.image.source="https://github.com/benc-uk/python-demoapp"

# Définir le répertoire de travail
WORKDIR /app

# Copier et installer les dépendances
ARG srcDir=src
COPY $srcDir/requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

# Copier les fichiers de l'application
COPY $srcDir/run.py .
COPY $srcDir/app ./app

# Exposer le port de l'application
EXPOSE 5000

# Définir la commande par défaut
CMD ["gunicorn", "-b", "0.0.0.0:5000", "run:app"]
